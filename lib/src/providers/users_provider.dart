import 'dart:convert';
import 'dart:io';

import 'package:path/path.dart';
import 'package:akanza_app/src/environment/environment.dart';
import 'package:akanza_app/src/models/response_api.dart';
import 'package:akanza_app/src/models/user.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;

class UsersProvider extends GetConnect {
  String url = Environment.API_URL;

  Future<Response> create(User user) =>
      post('$url/users/create', user.toJson(), contentType: 'application/json');

  Future<Stream> createUserWithImage(User user, File image) async {
    Uri uri = Uri.https(Environment.API_URL_OLD, '/users/createWithImage');
    final request = http.MultipartRequest('POST', uri);
    request.files.add(http.MultipartFile(
        'image', http.ByteStream(image.openRead().cast()), await image.length(),
        filename: basename(image.path)));
    request.fields['user'] = json.encode(user);
    final response = await request.send();
    return response.stream.transform(utf8.decoder);
  }

  Future<ResponseApi> createUserWithImageGetX(User user, File image) async {
    FormData form = FormData({
      "user": json.encode(user),
      "image": MultipartFile(image, filename: basename(image.path))
    });
    Response response = await post('$url/users/createWithImage', form,
        contentType: 'multipart/form-data');
    if (response.body == null) {
      Get.snackbar('Error en la peticion', 'No se pudo crear el usuario');
      return ResponseApi();
    }
    ResponseApi responseApi = ResponseApi.fromJson(response.body);
    return responseApi;
  }

  Future<ResponseApi> login(String email, String password) async {
    Response response = await post(
        '$url/auth/login',
        {
          'email': email,
          'password': password,
        },
        contentType: 'application/json');

    if (response.body == null) {
      Get.snackbar('Error', 'No se pudo conectar con el servidor');
      return ResponseApi();
    }

    ResponseApi responseApi = ResponseApi.fromJson(response.body);
    return responseApi;
  }
}
