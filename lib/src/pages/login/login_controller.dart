import 'dart:developer';

import 'package:akanza_app/src/models/response_api.dart';
import 'package:akanza_app/src/providers/users_provider.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

class LoginController extends GetxController {
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();

  UsersProvider usersProvider = UsersProvider();

  void goToRegisterPage() {
    Get.toNamed('/register');
  }

  void login() async {
    String email = emailController.text.trim();
    String password = passwordController.text.trim();

    if (isValidForm(email, password)) {
      ResponseApi responseApi = await usersProvider.login(email, password);
      if (responseApi.success == true) {
        GetStorage().write('user', responseApi.data);
        gotoHomePage();
      } else {
        Get.snackbar('Error', responseApi.message ?? '');
      }
    }
  }

  void gotoHomePage() {
    Get.offNamedUntil('/home', (route) => false);
  }

  bool isValidForm(String email, String password) {
    if (email.isEmpty) {
      Get.snackbar('Formulario Invalido', 'Debes ingresar el email');
      return false;
    }
    if (!GetUtils.isEmail(email)) {
      Get.snackbar('Formulario Invalido', 'Email inválido');
      return false;
    }
    if (password.isEmpty) {
      Get.snackbar('Formulario Invalido', 'Debes ingresar el password');
      return false;
    }
    return true;
  }
}
